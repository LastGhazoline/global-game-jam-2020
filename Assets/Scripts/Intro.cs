﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour
{
    public Sprite sp;
    public SpriteRenderer sr;

    private void Update()
    {
        if (sr.sprite == sp)
        {
            SceneManager.LoadScene("MainScene");
        }
    }
}
