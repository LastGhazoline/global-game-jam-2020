﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    float transparency = 0;
    Image texted;
    public Color a = new Color(1, 1, 1, 0);
    public Color b = new Color(1, 1, 1, 1);
    public bool sens = false;

    private void Awake()
    {
        sens = false;
        if (texted == null)
        {
            texted = GetComponent<Image>();
        }
        texted.color = a;
    }
    void Update()
    {
        if (!sens)
        {
            transparency += Time.deltaTime;
            texted.color = Color.Lerp(a, b, transparency);
        }
        else if (sens)
        {
            transparency -= Time.deltaTime;
            texted.color = Color.Lerp(a, b, transparency);
            if (transparency <= 0)
            {
                sens = false;
                gameObject.SetActive(false);
            }
        }
    }

    public void Reverse()
    {
        transparency = 1;
        sens = true;
    }
}
