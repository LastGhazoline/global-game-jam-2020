﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterBG : MonoBehaviour
{
    float transparency = 0;
    public Vector3 size = new Vector3(15, 10, 1);
    Vector3 star = new Vector3(00, 0, 00);
    bool over = false;
    GameManager gm;
    public bool sens = false;
    public Sprite[] sp;
    SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        int potato = Random.Range(0, sp.Length);

        sr.sprite = sp[potato];
        transform.localScale = new Vector3(0, 0, 0);
        gm = FindObjectOfType<GameManager>();
    }
    void Update()
    {
        if (transparency < 1 && !sens)
        {
            transparency += Time.deltaTime;

            transform.localScale = Vector3.Lerp(star, size, transparency);
        }

        else if (!over)
        {
            over = true;
            gm.SummonButton();
        }
        
        if (transparency > 0 && sens)
        {
            transparency -= Time.deltaTime;
                transform.localScale = Vector3.Lerp(star, size, transparency);
        }
    }
}
