﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterHuts : MonoBehaviour
{
    float transparency = 0;
    SpriteRenderer sr;
    public Color a;
    public Color b;
    public Sprite[] sp;
    bool sens = false;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        int potato = Random.Range(0, sp.Length);

        sr.sprite = sp[potato];
    }
    void Update()
    {
        if (transparency < 1 && !sens)
        {
            transparency += Time.deltaTime;
        }
        else if(transparency>0 && sens)
        {
            transparency -= Time.deltaTime;
        }
        
        sr.color = Color.Lerp(a, b, transparency);
    }

    public void Reverse()
    {
        sens=true;
    }
}
