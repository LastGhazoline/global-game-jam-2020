﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paquebots : MonoBehaviour
{
    public Vector3 targetG, targetD, currentTarget;
    float timer = 0;
    SpriteRenderer sr;
    public Color a, b;
    float transparency = 0;
    public Sprite[] sp;
    int speed;
    public int[] speeds;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        int potato = Random.Range(0, sp.Length);

        sr.sprite = sp[potato];

        
        
        timer = 0;
        speed = speeds[potato];
    }

    private void Update()
    {

        if (sr.color != b)
        {
            transparency += Time.deltaTime;
            sr.color = Color.Lerp(a, b, transparency);
        }
        else
        {
            if (Vector3.Distance(currentTarget, transform.position) < 0.1f)
            {
                if (currentTarget == targetG)
                {
                    currentTarget = targetD;
                }
                else
                {
                    currentTarget = targetG;
                }
                timer = 0;
            }
            else
            {
                var potato = Random.Range(6, 13) - speed;
                timer += Time.deltaTime / potato;
                transform.position = Vector3.Lerp(transform.position, currentTarget, timer);
            }
        }

        if (currentTarget.x < transform.position.x)
        {
            sr.flipX = false;
        }
        else
        {
            sr.flipX = true;
        }
    }
}
