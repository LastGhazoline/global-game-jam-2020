﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGWatRock : MonoBehaviour
{
    SpriteRenderer sr;
    public Color a, b;
    float transparency = 0;
    public Sprite[] sp;
    public bool sens = false;

    private void Awake()
    {
        
        if (sp.Length > 0)
        {
            sr=GetComponent<SpriteRenderer>();
            int potato = Random.Range(0, sp.Length);

            sr.sprite = sp[potato];
        }
        sr = GetComponent<SpriteRenderer>();
        

    }

    private void Update()
    {

        if (sr.color != b && !sens)
        {
            if (transparency < 1)
            {
                transparency += Time.deltaTime * 1.25f;
                sr.color = Color.Lerp(a, b, transparency);
            }
            
        }
        if(sr.color!=a && sens)
        {
            if (transparency > 0)
            {
                transparency -= Time.deltaTime * 1.25f;
                sr.color = Color.Lerp(a, b, transparency);
            }
        }
    }
}
