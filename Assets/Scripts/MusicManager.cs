﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    public AudioSource normal, rock, water, wood;
    public AudioSource ambiance, ambiance2;
    public AudioClip[] ambiant;
    int currentSource=0;


    void Update()
    {
        if (currentSource != 0)
        {
            if (normal.volume > 0)
            {
                normal.volume -= Time.deltaTime/4;
            }
        }

        if (currentSource == 1)
        {
            if (water.volume < 0.5f)
            {
                water.volume += Time.deltaTime/4;
            }
        }

        if (currentSource == 2)
        {
            if (wood.volume < 0.5f)
            {
                wood.volume += Time.deltaTime/4;
            }
        }

        if (currentSource == 3)
        {
            if (rock.volume < 0.5f)
            {
                rock.volume += Time.deltaTime/4;
            }
        }
    }

    public void Blu()
    {
        currentSource = 1;
    }

    public void Gren()
    {
        currentSource = 2;
    }

    public void Red()
    {
        currentSource = 3;
    }

    public void Ambiancer(int a)
    {
        ambiance.clip = ambiant[a];
        ambiance.Play();
    }

    public void Ambiancer2(int a)
    {
        ambiance2.clip = ambiant[a];
        ambiance2.Play();
    }
}
