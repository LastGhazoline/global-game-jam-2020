﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Woolooloo : MonoBehaviour
{
    public Vector3 target;
    float timer = 0;
    SpriteRenderer sr;
    public Color a, b;
    float transparency = 0;
    public Sprite[] sp;
    int speed;

    public Vector3 tourne;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        int potato = Random.Range(0, sp.Length);

        float x = Random.Range(-50, 51);
        x /= 10;
        float y = Random.Range(-35, 36);
        y /= 10;
        target = new Vector3(x, y, transform.position.z);
        timer = 0;
        speed = Random.Range(-3, 3);
    }

    private void Update()
    {

        if (sr.color != b)
        {
            transparency += Time.deltaTime;
            sr.color = Color.Lerp(a, b, transparency);
        }
        else
        {
            transform.Rotate(tourne*Time.deltaTime);
            if (Vector3.Distance(target, transform.position) < 0.1f)
            {
                float x = Random.Range(-83, 84);
                x /= 10;
                float y = Random.Range(-35, 54);
                y /= 10;
                target = new Vector3(x, y, transform.position.z);
                timer = 0;
            }
            else
            {
                var potato = Random.Range(6, 13) - speed;
                timer += Time.deltaTime / potato;
                transform.position = Vector3.Lerp(transform.position, target, timer);
            }
        }

        if (target.x < transform.position.x)
        {
            sr.flipX = false;
        }
        else
        {
            sr.flipX = true;
        }
    }
}
