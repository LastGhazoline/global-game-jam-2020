﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CouleurFiltred : MonoBehaviour
{
    public SpriteRenderer sr;
    public Color a, b;
    Color vide = new Color(0, 0, 0, 1);
    public float transparency = 0;
    GameManager gm;
    public bool ended=false, ended2=false;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        a = sr.color;
        b = Color.black;
    }

    // Update is called once per frame
    void Update()
    {
        if (b != Color.black && ended2==false)
        {
            transparency += Time.deltaTime;
            sr.color = Color.Lerp(a, b, transparency);
        }
        else if (ended2==true)
        {
            transparency -= Time.deltaTime;
            sr.color = Color.Lerp(a, b, transparency);
        }
    }

    public void SetColored(Color potato)
    {
        b = potato;
        a = new Color(potato.r, potato.g, potato.b, 0);
    }

    public void EndColor()
    {
        
        transparency = 0;
        a = sr.color;
        b = new Color(1, 1, 1, 1);
        ended = true;
    }

    public void EndColor2()
    {
        ended2 = true;
        transparency = 1;
        a = vide;
        b = sr.color;
    }
}
