﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTree : MonoBehaviour
{
    float transparency=0;
    SpriteRenderer sr;
    /*Color a = new Color(1, 1, 1, 0);
    Color b = new Color(1, 1, 1, 1);*/
    public Sprite[] sp;
    public Vector3 size = new Vector3(10, 10, 10);
    Vector3 star = new Vector3(10, 0, 10);
    bool sens = false;

    private void Awake()
    {
        sr = GetComponent < SpriteRenderer>();
        //sr.color = a;*/
        int potato = Random.Range(0, sp.Length);
        Debug.Log(potato);
        sr.sprite = sp[potato];

        transform.localScale = new Vector3(0, 0, 0);

            GetComponent<Animator>().SetBool(potato.ToString(), true);

       
    }
    void Update()
    {
        if (sr.sprite != null)
        {
            if (transparency < 1 && !sens)
            {
                transparency += Time.deltaTime;
            }
            if (transparency > 0 && sens)
            {
                transparency -= Time.deltaTime;
            }

            transform.localScale = Vector3.Lerp(star, size, transparency);
        }
        }

    public void RemoveTree()
    {
        Debug.Log("potato");
        sens = true;
    }
}
