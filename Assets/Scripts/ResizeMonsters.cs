﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResizeMonsters : MonoBehaviour
{

    Vector3 maxSize;
    Vector3 minSize = new Vector3(0, 0, 0);
    Vector3 mediumSize;
    bool sens=false;
    bool lanced=false;
    bool mini=false;
    float timer = 1;


    void Update()
    {
        if(sens==false && lanced && timer > 0)
        {
            timer -= Time.deltaTime;
            transform.localScale = Vector3.Lerp(minSize, mediumSize, timer);
        }
        else if(sens == false && lanced && timer <= 0 && !mini)
        {
            timer = 0;
            mini = true;
            StartCoroutine(WaitForIt());
        }

        if(sens && lanced && mini && timer < 1)
        {
            timer += Time.deltaTime;
            transform.localScale = Vector3.Lerp(minSize, maxSize, timer);
        }
    }

    public void Commencer()
    {
        var potato = Random.Range(15, 26);
        potato /= 10;
        mediumSize = transform.localScale;
        maxSize = transform.localScale * potato;
        lanced = true;
    }

    IEnumerator WaitForIt()
    {
        var potato = Random.Range(5, 16);
        potato /= 10;
        yield return new WaitForSeconds(potato);
        sens = true;
    }
}
