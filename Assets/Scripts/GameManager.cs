﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    //public static GameManager _instance { get; private set; }



    [Header("Meta")]
    public GameObject sneakButton;
    public GameObject choiceParent;
    public GameObject choiceParent2;
    public GameObject FuckingButton;
    public UI[] bg;
    public Text[] choices;
    bool waiting = false;
    bool returning=false;
    public int choiceChoice;

    int choice1=-1, choice2=-1, choice3=-1;

    public CouleurFiltred filtreCouleur;

    bool ending = false;
    MusicManager mm;
    public Sprite[] UI;
    public Image question;
    public Image[] reponses;

    AudioSource audios;
    public AudioClip[] sounds;

    [Header("Forest")]
    public GameObject forest1;
    public List<GameObject> forestanimals;
    public List<GameObject> forestH;
    public List<GameObject> forestWoo;
    public List<GameObject> forestAnt;
    public List<GameObject> backUpFH;
    public List<GameObject> forestHS;
    public List<GameObject> moreTrees;
    public GameObject colline;
    public GameObject montagne;
    public List<GameObject> collineMobs;
    public List<GameObject> forestIndus;
    public List<GameObject> backupTrees;
    public List<GameObject> killTrees;

    [Header("Water")]
    public GameObject water1;
    public List<GameObject> waterH;
    public List<GameObject> waterA;
    public List<GameObject> waterE;
    public List<GameObject> waterHS;
    public List<GameObject> backupWaterH;
    public List<GameObject> waterHC;
    public List<GameObject> waterAPro;
    public List<GameObject> waterAPreSiz;
    public GameObject baleine;
    public List<GameObject> backupIslands;
    public List<GameObject> waterEA;
    public List<GameObject> waterEC;

    [Header("Rock")]
    public List<GameObject> rock1;
    public List<GameObject> rockH;
    public List<GameObject> rockA;
    public GameObject rockE;
    public List<GameObject> rockSize;
    public List<GameObject> rockA2;
    public GameObject rockAPre;
    public List<GameObject> rockHS;
    public List<GameObject> rockHC;
    public List<GameObject> backupRock;
    public GameObject rockEA;
    public GameObject rockEC;

    [Header("Ending")]
    public GameObject endParent;
    public Text endText;
    public string[] endings;
    int whatEnd=-1;
    public Color[] filters;

    private void Start()
    {
        audios = GetComponent<AudioSource>();
        mm = GetComponent<MusicManager>();
    }
   

    public void NextChoice()
    {
        if (ending == false)
        {
            sneakButton.SetActive(false);
            choiceParent.SetActive(true);
            choiceParent.GetComponent<ParentUI>().Nik();
            choiceParent2.SetActive(true);
        }
        

        for (int i = 0; i < choices.Length; i++)
        {
            choices[i].gameObject.SetActive(true);
            choices[i].text = "";
        }
        

        NewChoiceText();
    }

    public void SummonButton()
    {
        sneakButton.SetActive(true);
    }

    void NewChoiceText()
    {
        if (choice1 == -1)
        {
            choices[0].text = "What is your favourite colour ?";
            choices[1].text = "Blue";
            choices[2].text = "Green";
            choices[3].text = "Red";

        }
        else if (choice2 == -1)
        {
            choices[0].text = "What do you do when you feel stressed ?";
            choices[1].text = "Take a walk in the city";
            choices[2].text = "Explore the surrounding nature.";
            choices[3].text = "Go climb things";
        }
        else if (choice3 == -1)
        {
            if (choice2 == 0)
            {
                choices[0].text = "What is better ?";
                choices[1].text = "Civilization";
                choices[2].text = "Harmony";
                choices[3].text = "";
            }
            else if(choice2 == 1)
            {
                choices[0].text = "What kind of person are you ?";
                choices[1].text = "Predator";
                choices[2].text = "Pacifist";
                choices[3].text = "";
            }
            else if (choice2 == 2)
            {
                choices[0].text = "How do you handle conflict ?";
                choices[1].text = "Aggressively";
                choices[2].text = "Peacefully";
                choices[3].text = "";
            }

            FuckingButton.SetActive(false);
        }
        else
        {
            TheEnd();
        }
    }

    public void Choisir(int choiceID)
    {
        Clicked();
        if (choice1 == -1)
        {
            choiceChoice = 1;
            choice1 = choiceID;
            filtreCouleur.SetColored(filters[choiceID]);
            
        }
        else if (choice2 == -1)
        {
            choiceChoice = 2;
            choice2 = choiceID;
        }
        else if (choice3 == -1)
        {
            choiceChoice = 3;
            choice3 = choiceID;
        }
        waiting = true;
        
        for (int i = 0; i < choices.Length; i++)
        {
            choices[i].GetComponent<InvokeText>().Reverse();
        }
        for (int i = 0; i < bg.Length; i++)
        {
            bg[i].Reverse();
        }
    }

    void FirstChoice()
    {
        if (choice1 == 0)
        {
            for (int i = 0; i < reponses.Length; i++)
            {
                reponses[i].sprite = UI[3];
            }
            question.sprite = UI[0];
            water1.SetActive(true);
            mm.Ambiancer(4);
            mm.Blu();
        }
        else if (choice1 == 1)
        {
            for (int i = 0; i < reponses.Length; i++)
            {
                reponses[i].sprite = UI[4];
            }
            question.sprite = UI[1];
            mm.Ambiancer(6);
            forest1.SetActive(true);
            mm.Gren();
            
        }
        else if (choice1 == 2)
        {
            for (int i = 0; i < reponses.Length; i++)
            {
                reponses[i].sprite = UI[5];
            }
            question.sprite = UI[2];
            mm.Ambiancer(7);
            StartCoroutine(SolTerre());
            mm.Red();
        }
    }

    void SecondChoice()
    {
        //human
        if (choice2 == 0)
        {
            
            //water
            if (choice1 == 0)
            {
                StartCoroutine(WaterHut());
            }
            //forest
            else if (choice1 == 1)
            {
                StartCoroutine(ForestHut());
            }
            //rock
            else if (choice1 == 2)
            {
                mm.Ambiancer2(0);
                StartCoroutine(RockHut());
            }
        }

        //animal
        if (choice2 == 1)
        {
            if (choice1 == 0)
            {
                StartCoroutine(WaterMonsters());
            }
            else if (choice1 == 1)
            {
                mm.Ambiancer2(5);
                StartCoroutine(Cureuils());
            }
            else if (choice1 == 2)
            {
                StartCoroutine(Steppes());
            }
        }

        //escalade
        if (choice2 == 2)
        {
            if (choice1 == 0)
            {
                StartCoroutine(WaterIsle());
            }
            else if (choice1 == 1)
            {
                StartCoroutine(TreeKiller());
                
            }
            else if (choice1 == 2)
            {
                mm.Ambiancer2(1);
                rockE.SetActive(true);
            }
        }
        sneakButton.SetActive(true);
    }

    void ThirdChoice()
    {

        if (choice3 == 0)
        {
            //Indus
            if (choice2 == 0)
            {
                if (choice1 == 0)
                {
                    StartCoroutine(RemoveWaterHutsHC());
                    mm.Ambiancer(2);
                    whatEnd = 0;
                }
                else if (choice1 == 1)
                {
                    mm.Ambiancer2(0);
                    forestHS = forestIndus;
                    StartCoroutine(NoForest());
                    whatEnd = 10;
                    whatEnd = 1;
                }
                else if (choice1 == 2)
                {
                    rockH = rockHC;
                    StartCoroutine(FutureRockHuts());
                    whatEnd = 2;
                }
            }

            //Préda
            if (choice2 == 1)
            {
                if (choice1 == 0)
                {
                    rockSize= waterAPreSiz;
                    StartCoroutine(ResizeRocks());
                    baleine.SetActive(true);
                    whatEnd = 3;
                }
                else if (choice1 == 1)
                {
                    StartCoroutine(BigForest());
                    whatEnd = 4;
                }
                else if (choice1 == 2)
                {
                    StartCoroutine(ResizeRocks());
                    rockAPre.SetActive(true);
                    whatEnd = 5;
                }
            }

            //Agressif
            if (choice2 == 2)
            {
                if (choice1 == 0)
                {
                    StartCoroutine(RemoveIslands());
                    whatEnd = 6;
                }
                else if (choice1 == 1)
                {
                    montagne.SetActive(true);
                    whatEnd = 7;
                }
                else if (choice1 == 2)
                {
                    rockEA.SetActive(true);
                    //rockE.SetActive(false);
                    whatEnd = 8;
                }
            }
        }
        else if (choice3 == 1)
        {
            //Symbiose
            if (choice2 == 0)
            {
                if (choice1 == 0)
                {
                    StartCoroutine(RemoveWaterHuts());
                    whatEnd = 9;
                }
                else if (choice1 == 1)
                {
                    StartCoroutine(NewForest());
                    StartCoroutine(FutureHuts());
                    whatEnd = 10;
                }
                else if (choice1 == 2)
                {
                    rockH = rockHS;
                    StartCoroutine(FutureRockHuts());
                    whatEnd = 11;
                }
            }

            //Proie
            if (choice2 == 1)
            {
                if (choice1 == 0)
                {
                    mm.Ambiancer2(3);
                    waterA = waterAPro;
                    StartCoroutine(WaterMonsters());
                    whatEnd = 12;
                }
                else if (choice1 == 1)
                {
                    
                    StartCoroutine(Wooloo());
                    whatEnd = 13;
                }
                else if (choice1 == 2)
                {
                    rockA = rockA2;
                    StartCoroutine(Steppes());
                    whatEnd = 14;
                }
            }

            //Concilliant
            if (choice2 == 2)
            {
                if (choice1 == 0)
                {
                    waterEA = waterEC;
                    StartCoroutine(RemoveIslands());
                    whatEnd = 15;
                }
                else if (choice1 == 1)
                {
                    forestanimals = collineMobs;
                    StartCoroutine(Cureuils());
                    whatEnd = 16;
                }
                else if (choice1 == 2)
                {
                    rockEC.SetActive(true);
                    rockE.SetActive(false);
                    whatEnd = 17;
                }
            }
        }
        sneakButton.SetActive(true);
    }


    void TheEnd()
    {
        if (!endParent.activeSelf && !returning)
        {
            ending = true;
            Debug.Log("1");
            choiceParent2.SetActive(false);
            choiceParent.SetActive(false);
            endParent.SetActive(true);
            endText.text = endings[whatEnd];
            sneakButton.SetActive(true);
        }
        else if(!returning)
        {
            Debug.Log("2");
            endParent.SetActive(false);
            endText.text = endings[whatEnd];
            returning = true;
        }
        else
        {
            Debug.Log("3");
            sneakButton.SetActive(false);
            filtreCouleur.EndColor();
        }
    }

    public void Clicked()
    {
        audios.clip = sounds[0];
        audios.Play();
    }

    private void Update()
    {
        if(waiting == true)
        {
            if (!choices[0].gameObject.activeSelf)
            {
                choiceParent.SetActive(false);
                choiceParent2.SetActive(false);
                waiting = false;
                if (choiceChoice == 1)
                {
                    FirstChoice();
                }
                else if (choiceChoice == 2)
                {
                    SecondChoice();
                }
                else if (choiceChoice == 3)
                {
                    ThirdChoice();
                }
            }
        }

        if(filtreCouleur.transparency>=1 && filtreCouleur.ended)
        {
            filtreCouleur.EndColor2();
        }
        if(filtreCouleur.transparency<=0 && filtreCouleur.ended2)
        {
            GetBack();
        }
    }

    public void GetBack()
    {
        SceneManager.LoadScene("MainMenu");
    }

    IEnumerator Cureuils()
    {
        int potato = Random.Range(0, forestanimals.Count);
        forestanimals[potato].SetActive(true);
        forestanimals.RemoveAt(potato);
        float time = Random.Range(5, 12);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (forestanimals.Count > 0)
        {
            StartCoroutine(Cureuils());
        }
        else
        {
            SummonButton();
        }
        
    }

    IEnumerator SolTerre()
    {
        int potato = Random.Range(0, rock1.Count);
        rock1[potato].SetActive(true);
        rock1.RemoveAt(potato);
        float time = Random.Range(1, 6);
        time /= 10;
        yield return new WaitForSeconds(time);
        if (rock1.Count > 0)
        {
            StartCoroutine(SolTerre());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator WaterHut()
    {
        int potato = Random.Range(0, waterH.Count);
        waterH[potato].SetActive(true);
        waterH.RemoveAt(potato);
        float time = Random.Range(5, 12);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (waterH.Count > 0)
        {
            StartCoroutine(WaterHut());
        }
        else
        {
            SummonButton();
        }

    }

    IEnumerator WaterIsle()
    {
        int potato = Random.Range(0, waterE.Count);
        waterE[potato].SetActive(true);
        waterE.RemoveAt(potato);
        float time = Random.Range(5, 12);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (waterE.Count > 0)
        {
            StartCoroutine(WaterIsle());
        }
        else
        {
            SummonButton();
        }

    }

    IEnumerator WaterMonsters()
    {
        int potato = Random.Range(0, waterA.Count);
        waterA[potato].SetActive(true);
        waterA.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (waterA.Count > 0)
        {
            StartCoroutine(WaterMonsters());
        }
        else
        {
            SummonButton();
        }

    }

    IEnumerator ForestHut()
    {
        int potato = Random.Range(0, forestH.Count);
        forestH[potato].SetActive(true);
        forestH.RemoveAt(potato);
        float time = Random.Range(5, 12);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (forestH.Count > 0)
        {
            StartCoroutine(ForestHut());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator RockHut()
    {
        int potato = Random.Range(0, rockH.Count);
        rockH[potato].SetActive(true);
        rockH.RemoveAt(potato);
        float time = Random.Range(1, 6);
        time /= 10;
        yield return new WaitForSeconds(time);
        if (rockH.Count > 0)
        {
            StartCoroutine(RockHut());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator Steppes()
    {
        int potato = Random.Range(0, rockA.Count);
        rockA[potato].SetActive(true);
        rockA.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (rockA.Count > 0)
        {
            StartCoroutine(Steppes());
        }
        else
        {
            SummonButton();
        }

    }

    IEnumerator Wooloo()
    {

        int potato = Random.Range(0, forestWoo.Count);
        forestWoo[potato].SetActive(true);
        forestWoo.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (forestWoo.Count > 0)
        {
            StartCoroutine(Wooloo());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator BigForest()
    {
        int potato = Random.Range(0, forestAnt.Count);
        forestAnt[potato].SetActive(true);
        forestAnt.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (forestAnt.Count > 0)
        {
            StartCoroutine(BigForest());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator FutureHuts()
    {
        int potato = Random.Range(0, forestHS.Count);
        forestHS[potato].SetActive(true);
        backUpFH[potato].GetComponent<RandomTree>().RemoveTree();
        backUpFH.RemoveAt(potato);
        forestHS.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (forestHS.Count > 0)
        {
            StartCoroutine(FutureHuts());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator NewForest()
    {
        int potato = Random.Range(0, moreTrees.Count);
        moreTrees[potato].SetActive(true);
        moreTrees.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (moreTrees.Count > 0)
        {
            StartCoroutine(NewForest());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator NoForest()
    {
        int potato = Random.Range(0, backupTrees.Count);
        backupTrees[potato].GetComponent<RandomTree>().RemoveTree();
        backupTrees.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (backupTrees.Count > 0)
        {
            StartCoroutine(NoForest());
        }
        else
        {
            StartCoroutine(FutureHuts());
        }
    }


    IEnumerator RemoveWaterHuts()
    {
        int potato = Random.Range(0, backupWaterH.Count);
        backupWaterH[potato].GetComponent<WaterHuts>().Reverse();
        backupWaterH.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (backupWaterH.Count > 0)
        {
            StartCoroutine(RemoveWaterHuts());
        }
        else
        {
            StartCoroutine(SummonWaterHS());
        }
    }

    IEnumerator RemoveWaterHutsHC()
    {
        int potato = Random.Range(0, backupWaterH.Count);
        backupWaterH[potato].GetComponent<WaterHuts>().Reverse();
        backupWaterH.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (backupWaterH.Count > 0)
        {
            StartCoroutine(RemoveWaterHutsHC());
        }
        else
        {
            StartCoroutine(SummonWaterHC());
        }
    }
    IEnumerator SummonWaterHC()
    {
        int potato = Random.Range(0, waterHC.Count);
        waterHC[potato].SetActive(true);
        waterHC.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (waterHC.Count > 0)
        {
            StartCoroutine(SummonWaterHC());
        }
        else
        {
            SummonButton();
        }
    }


    IEnumerator SummonWaterHS()
    {
        int potato = Random.Range(0, waterHS.Count);
        waterHS[potato].SetActive(true);
        waterHS.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (waterHS.Count > 0)
        {
            StartCoroutine(SummonWaterHS());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator ResizeRocks()
    {
        int potato = Random.Range(0, rockSize.Count);
        rockSize[potato].GetComponent<ResizeMonsters>().Commencer();
        rockSize.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (rockSize.Count > 0)
        {
            StartCoroutine(ResizeRocks());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator FutureRockHuts()
    {
        int potato = Random.Range(0, rockH.Count);
        rockH[potato].SetActive(true);
        backupRock[potato].GetComponent<BGWatRock>().sens = true;
        rockH.RemoveAt(potato);
        backupRock.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (rockH.Count > 0)
        {
            StartCoroutine(FutureRockHuts());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator RemoveIslands()
    {
        int potato = Random.Range(0, backupIslands.Count);
        backupIslands[potato].GetComponent<WaterBG>().sens=true;
        backupIslands.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (backupIslands.Count > 0)
        {
            StartCoroutine(RemoveIslands());
        }
        else
        {
            StartCoroutine(SummonIslandsE());
        }
    }

    IEnumerator SummonIslandsE()
    {
        int potato = Random.Range(0, waterEA.Count);
        waterEA[potato].SetActive(true);
        waterEA.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (waterEA.Count > 0)
        {
            StartCoroutine(SummonIslandsE());
        }
        else
        {
            SummonButton();
        }
    }

    IEnumerator TreeKiller()
    {
        int potato = Random.Range(0, killTrees.Count);
        killTrees[potato].GetComponent<RandomTree>().RemoveTree();
        killTrees.RemoveAt(potato);
        float time = Random.Range(10, 25);
        time /= 100;
        yield return new WaitForSeconds(time);
        if (killTrees.Count > 0)
        {
            StartCoroutine(TreeKiller());
        }
        else
        {
            colline.SetActive(true);
        }
    }
}
