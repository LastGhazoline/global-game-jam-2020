﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroAnim : MonoBehaviour
{

    public AudioSource son, music;
    public AudioClip sonded;

    public void Animationer()
    {
        GetComponent<Animator>().SetTrigger("Death");
        son.clip = sonded;
        son.Play();
        music.Stop();
    }


}
