﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteppesMonsters : MonoBehaviour
{
    public Vector3 target;
    float timer = 0;
    SpriteRenderer sr;
    public Color a, b;
    float transparency = 0;
    public Sprite[] sp;
    int speed;
    public int[] speeds;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        int potato = Random.Range(0, sp.Length);

        sr.sprite = sp[potato];

        float x = Random.Range(-83, 84);
        x /= 10;
        float y = Random.Range(-35, 54);
        y /= 10;
        target = new Vector3(x, y, transform.position.z);
        timer = 0;
        speed = speeds[potato];
        GetComponent<Animator>().SetBool(potato.ToString(), true);
    }

    private void Update()
    {

        if (sr.color != b)
        {
            transparency += Time.deltaTime;
            sr.color = Color.Lerp(a, b, transparency);
        }
        else
        {
            if (Vector3.Distance(target, transform.position) < 0.1f)
            {
                float x = Random.Range(-83, 84);
                x /= 10;
                float y = Random.Range(-35, 54);
                y /= 10;
                while(x>0  && y < 0)
                {
                    x = Random.Range(-83, 84);
                    x /= 10;
                    y = Random.Range(-35, 54);
                    y /= 10;
                }
                target = new Vector3(x, y, transform.position.z);
                timer = 0;
            }
            else
            {
                var potato = Random.Range(6, 13) - speed;
                timer += Time.deltaTime / potato;
                transform.position = Vector3.Lerp(transform.position, target, timer);
            }
        }

        if (target.x < transform.position.x)
        {
            sr.flipX = true;
        }
        else
        {
            sr.flipX = false;
        }
    }
}
