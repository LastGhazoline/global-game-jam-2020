﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundForest : MonoBehaviour
{
    public List<GameObject> trees;
    GameManager gm;

    SpriteRenderer sr;
    public Color a, b;
    float transparency = 0;

    bool invoque = false;

    void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (sr.color!=b)
        {
            transparency += Time.deltaTime;
            sr.color = Color.Lerp(a, b, transparency);
        }
        if(sr.color==b && !invoque)
        {
            invoque = true;
            StartCoroutine(GrowTrees());
        }
    }

    IEnumerator GrowTrees()
    {
        int potato = Random.Range(0, trees.Count);
        trees[potato].SetActive(true);
        trees.RemoveAt(potato);
        float time = Random.Range(1, 6);
        time /= 10;
        yield return new WaitForSeconds(time);
        if (trees.Count > 0)
        {
            StartCoroutine(GrowTrees());
        }
        else
        {
            gm.SummonButton();
        }
    }
}
